<?php

use App\Contabilidad\Informe;
use Illuminate\Container\Container;
use App\Contabilidad\InformeContable;
use App\Contabilidad\Export\InformeWeb;
use App\Contabilidad\Export\InformeJson;
use App\Contabilidad\EstructuraBaseInforme;
use App\Contabilidad\Export\InformeConsola;

require_once __DIR__ . '/vendor/autoload.php';

// $project = new ProjectManagement();
// $tester = new Tester();
// echo $project->process($tester);

// Inveversion of control
$container = new Container();

// Open-Close
// $container->bind('project', 'App\Programation\Refactored\ProjectManagement');
// $container->bind('tester', 'App\Programation\Refactored\Tester');
// $container->bind('developer', 'App\Programation\Refactored\Programmer');

$container->bind('informe', function ($container) {
    return new InformeContable("1", "3", true);
});

$informe = $container->make('informe');

$container->bind('base', function ($container) use ($informe) {
    return new EstructuraBaseInforme($informe);
});
$base = $container->make('base');

$container->bind('json', function ($container) use ($base) {
    return new InformeJson($base);
});

$json = $container->make('json');

$container->bind('web', function ($container) use ($base) {
    return new InformeWeb($base);
});

$web = $container->make('web');

$container->bind('consola', function ($container) use ($base) {
    return new InformeConsola($base);
});

$consola = $container->make('consola');

//echo $json->visualizar($base);

$informe->generarInformeContable($consola);
