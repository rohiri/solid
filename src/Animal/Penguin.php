<?php

namespace App\Animal;

class Penguin extends Bird
{
    public function eat()
    {
        return 'Penguin eating';
    }
}
