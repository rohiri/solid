<?php

namespace App\Animal;

class FlyBird extends Bird
{
    public function fly()
    {
        return 'flying';
    }
}
