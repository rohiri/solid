<?php

namespace App\Animal;

class Bird
{
    public function eat()
    {
        return 'eating';
    }

    public function tweet()
    {
        return 'tweeting';
    }
}
