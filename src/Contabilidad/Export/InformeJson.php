<?php

namespace App\Contabilidad\Export;

use App\Contabilidad\Informe;
use Illuminate\Support\Collection;
use App\Contabilidad\Concern\InformeComun;
use App\Contabilidad\EstructuraBaseInforme;

class InformeJson extends Informe implements InformeComun
{
    public function visualizar()
    {
        return json_encode($this->base->baseInforme(), 200, JSON_PRETTY_PRINT);
    }
}
