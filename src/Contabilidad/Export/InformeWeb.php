<?php

namespace App\Contabilidad\Export;

use App\Contabilidad\Informe;
use App\Contabilidad\Concern\InformeComun;

class InformeWeb extends Informe implements InformeComun
{
    public function visualizar()
    {
        dd($this->base->baseInforme());
    }
}
