<?php

namespace App\Contabilidad\Export;

use App\Contabilidad\Informe;
use App\Contabilidad\Concern\InformeComun;

class InformeConsola extends Informe implements InformeComun
{
    public function visualizar()
    {
        echo "Reporte en Consola";
    }
}
