<?php

namespace App\Contabilidad;

use App\Contabilidad\InformeContable;

class EstructuraBaseInforme
{
    private ?string $cuenta;

    private float $debito;

    private float $credito;

    private array $data = [];


    public function __construct(InformeContable $informeContable)
    {
        $this->cuenta = $informeContable->cuentaInicial;
        $this->debito = $informeContable->debito;
        $this->credito = $informeContable->credito;
    }

    public function baseInforme()
    {
        for ($i = 1; $i <= 10; $i++) {
            $this->data[] = [
                'Cuenta' => $this->cuenta,
                'Debito' => $this->debito,
                'Credito' => $this->credito,
            ];
        }
        return $this->data;
    }
}
