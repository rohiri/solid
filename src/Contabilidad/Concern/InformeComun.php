<?php

namespace App\Contabilidad\Concern;

interface InformeComun
{
    public function visualizar();
}
