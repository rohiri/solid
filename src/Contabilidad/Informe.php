<?php

namespace App\Contabilidad;

abstract class Informe
{
    public $base;

    public function __construct(EstructuraBaseInforme $base)
    {
        $this->base = $base;
    }
}
