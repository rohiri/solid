<?php

namespace App\Contabilidad;

use App\Contabilidad\Concern\InformeComun;

class InformeContable
{
    public ?string $cuentaInicial;

    public ?string $cuentaFinal;

    public float $debito;

    public float $credito;

    public bool $detallado;

    public function __construct(string $cuentaInicial, string $cuentaFinal, bool $detallado)
    {
        $this->cuentaInicial = $cuentaInicial;
        $this->cuentaFinal = $cuentaFinal;
        $this->detallado = $detallado;
        $this->debito = 100;
        $this->credito = -100;
    }

    public function generarInformeContable(InformeComun $report)
    {
        echo $report->visualizar();
    }
}
