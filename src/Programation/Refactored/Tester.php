<?php

namespace App\Programation\Refactored;

use App\Programation\Refactored\Concern\Workable;

class Tester implements Workable
{
    public function work()
    {
        return 'testing';
    }
}
