<?php

namespace App\Programation\Refactored;

use App\Programation\Refactored\Concern\Workable;

class Programmer implements Workable
{
    public function work()
    {
        return 'coding';
    }
}
