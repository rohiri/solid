<?php

namespace App\Programation\Refactored;

use App\Programation\Refactored\Concern\Workable;

class ProjectManagement
{
    public function process(Workable $member)
    {
        return $member->work();
    }
}
