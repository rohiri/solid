<?php

namespace App\Programation\Refactored\Concern;

interface Workable
{
    public function work();
}
